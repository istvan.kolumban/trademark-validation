﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace prepare_trademark_files
{
    class Program
    {
        static void Main(string[] args)
        {
            string pathToXmlFiles = @"E:\Informatika\vacuumlabs\Interview 3\trademarks";

            string pathToSaveFiles = Path.Combine(pathToXmlFiles, "preprocessed-trademarks");
            Directory.CreateDirectory(pathToSaveFiles);

            string[] files = Directory.GetFiles(pathToXmlFiles, "*.xml", SearchOption.AllDirectories);

            foreach (var filePath in files)
            {
                var doc = XElement.Load(filePath);
                XElement xmlDocumentWithoutNs = RemoveAllNamespaces(doc);
                xmlDocumentWithoutNs.Save(Path.Combine(pathToSaveFiles, Path.GetFileName(filePath)));
            }
        }

        private static XElement RemoveAllNamespaces(XElement xmlDocument)
        {
            if (!xmlDocument.HasElements)
            {
                XElement xElement = new XElement(xmlDocument.Name.LocalName);
                xElement.Value = xmlDocument.Value;

                foreach (XAttribute attribute in xmlDocument.Attributes())
                    xElement.Add(attribute);

                return xElement;
            }
            return new XElement(xmlDocument.Name.LocalName, xmlDocument.Elements().Select(el => RemoveAllNamespaces(el)));
        }
    }
}
