﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using trademark_validation.DTOs;
using trademark_validation.Models;
using trademark_validation.Repositories;

namespace trademark_validation.Services
{
    public class TrademarksService : ITrademarksService
    {
        private readonly ITrademarkRepository _repository;
        private readonly ICalculateSimilarityService _calculateSimilarityService;
        private readonly ILogger<TrademarksService> _logger;
        private readonly IMapper _mapper;
        public TrademarksService(ICalculateSimilarityService calculateSimilarityService, ITrademarkRepository repository, ILogger<TrademarksService> logger, IMapper mapper)
        {
            _calculateSimilarityService = calculateSimilarityService;
            _repository = repository;
            _logger = logger;
            _mapper = mapper;
        }

        public List<TrademarkDTO> GetExactMatches(string searchedText)
        {
            try
            {
                List<Trademark> trademarks = _repository.GetExactMatches(searchedText);
                if (trademarks != null)
                {
                    List<TrademarkDTO> trademarkDTOs = ConvertTrademarksToTrademarkDTOs(trademarks);
                    return trademarkDTOs;
                }
                else
                {
                    return new List<TrademarkDTO>();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Faild to get trademarks (Service): {ex}");
                throw;
            }
        }

        public SortedList<int, List<TrademarkDTO>> GetNearestMatches(string searchedText)
        {
            try
            {
                List<Trademark> trademarks = _repository.GetAllTrademarks();
                int maxNumberOfLevels = 4;
                int minPercentage = 65;
                SortedList<int, List<Trademark>> nearestTrademarks = GetNearestTrademarks(trademarks, searchedText.ToLower(), maxNumberOfLevels, minPercentage);
                var nearestTrademarkDTOs = new SortedList<int, List<TrademarkDTO>>();
                foreach(var (key, value) in nearestTrademarks)
                {
                    nearestTrademarkDTOs.Add(key, ConvertTrademarksToTrademarkDTOs(value));
                }
                return nearestTrademarkDTOs;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Faild to get trademarks (Service): {ex}");
                throw;
            }
        }

        public Dictionary<string, List<TrademarkDTO>> GetContainsMatches(string searchedText)
        {
            try
            {
                var containsMatches = new Dictionary<string, List<TrademarkDTO>>();

                // search for exact match, no case sensitive
                List<Trademark> trademarks = _repository.GetExactMatchesNoCaseSensitive(searchedText);
                containsMatches.Add("NoCaseSensitive", ConvertTrademarksToTrademarkDTOs(trademarks));

                // search for trademarks which contain the searched text, no case sensitive
                trademarks = _repository.GetTrademarksContainSearchedText(searchedText);
                containsMatches.Add("TrademarkContains", ConvertTrademarksToTrademarkDTOs(trademarks));

                // search for trademarks which are in searched text, no case sensitive
                trademarks = _repository.GetTrademarksInSearchedText(searchedText);
                containsMatches.Add("SearchedTextContains", ConvertTrademarksToTrademarkDTOs(trademarks));
                return containsMatches;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Faild to get trademarks (Service): {ex}");
                throw;
            }
        }

        private SortedList<int, List<Trademark>> GetNearestTrademarks(List<Trademark> trademarks, string searchedText, int maxNumberOfLevels, int minPercentage)
        {
            var nearestTrademarks = new SortedList<int, List<Trademark>>();
            foreach(var trademark in trademarks)
            {
                double score = _calculateSimilarityService.Calculate(searchedText, trademark.MarkVerbalElementText.ToLower());
                int percentage = (int)Math.Round(score * 100);
                if (percentage >= minPercentage)
                {
                    if (nearestTrademarks.ContainsKey(percentage))
                    {
                        nearestTrademarks[percentage].Add(trademark);
                    }
                    else
                    {
                        nearestTrademarks.Add(percentage, new List<Trademark> { trademark });
                    }
                    if (nearestTrademarks.Count > maxNumberOfLevels)
                    {
                        nearestTrademarks.RemoveAt(0);
                    }
                }
            }
            return nearestTrademarks;
        }

        private List<TrademarkDTO> ConvertTrademarksToTrademarkDTOs(List<Trademark> trademarks)
        {
            List<TrademarkDTO> trademarkDTOs = new List<TrademarkDTO>();
            {
                foreach (var trademark in trademarks)
                {
                    var trademarkDTO = _mapper.Map<Trademark, TrademarkDTO>(trademark);

                    if (trademarkDTO.ExpiryDate == null)
                    {
                        trademarkDTO.State = "Unknown";
                    }
                    else if (trademarkDTO.ExpiryDate >= DateTime.Now)
                    {
                        trademarkDTO.State = "Live";
                    }
                    else
                    {
                        trademarkDTO.State = "Expired";
                    }
                    trademarkDTOs.Add(trademarkDTO);
                }
            }

            return trademarkDTOs;
        }
    }
}
