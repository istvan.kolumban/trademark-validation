﻿using System.Collections.Generic;
using trademark_validation.DTOs;
using trademark_validation.Models;

namespace trademark_validation.Services
{
    public interface ITrademarksService
    {
        List<TrademarkDTO> GetExactMatches(string searchedText);
        SortedList<int, List<TrademarkDTO>> GetNearestMatches(string searchedText);
        Dictionary<string, List<TrademarkDTO>> GetContainsMatches(string searchedText);
    }
}
