﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using trademark_validation.Models;

namespace trademark_validation.Repositories
{
    public interface ITrademarkRepository
    {
        List<Trademark> GetExactMatches(string searchedText);
        List<Trademark> GetTrademarksContainSearchedText(string searchedText);
        List<Trademark> GetTrademarksInSearchedText(string searchedText);
        List<Trademark> GetExactMatchesNoCaseSensitive(string searchedText);
        List<Trademark> GetAllTrademarks();
    }
}
