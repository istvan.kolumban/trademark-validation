﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using trademark_validation.Models;

namespace trademark_validation.Repositories
{
    public class TrademarkManagerContext : DbContext
    {
        private readonly IConfiguration _config;
        public DbSet<Trademark> Trademarks { get; set; }

        public TrademarkManagerContext(IConfiguration config)
        {
            _config = config;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.UseSqlServer(_config["ConnectionStrings:TrademarkManagerContextDb"]);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

    }
}
