﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using trademark_validation.Models;

namespace trademark_validation.Repositories
{
    public class TrademarkRepository : ITrademarkRepository
    {
        private readonly TrademarkManagerContext _ctx;
        private readonly ILogger<TrademarkRepository> _logger;

        public TrademarkRepository(TrademarkManagerContext ctx, ILogger<TrademarkRepository> logger)
        {
            _ctx = ctx;
            _logger = logger;
        }

        public List<Trademark> GetExactMatches(string searchedText)
        {
            try
            {
                var trademarks = _ctx.Trademarks
                    .Where((Trademark trademark) => 
                        trademark.MarkVerbalElementText.Equals(searchedText) &&
                        trademark.MarkFeature.Equals("Word"))
                    .ToList<Trademark>();

                if (trademarks != null)
                {
                    return trademarks;
                }
                return new List<Trademark>();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Faild to get trademarks (Repository): {ex}");
                throw;
            }
        }

        public List<Trademark> GetExactMatchesNoCaseSensitive(string searchedText)
        {
            try
            {
                var trademarks = _ctx.Trademarks
                    .Where((Trademark trademark) => 
                        trademark.MarkVerbalElementText.ToLower().Equals(searchedText.ToLower()) &&
                        trademark.MarkFeature.Equals("Word"))
                    .ToList<Trademark>();

                if (trademarks != null)
                {
                    return trademarks;
                }
                return new List<Trademark>();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Faild to get trademarks (Repository): {ex}");
                throw;
            }
        }

        public List<Trademark> GetTrademarksContainSearchedText(string searchedText)
        {
            try
            {
                var trademarks = _ctx.Trademarks
                    .Where((Trademark trademark) => 
                        trademark.MarkVerbalElementText.ToLower().Contains(searchedText.ToLower()) && 
                        !trademark.MarkVerbalElementText.ToLower().Equals(searchedText.ToLower()) &&
                        trademark.MarkFeature.Equals("Word"))
                    .ToList<Trademark>();

                if (trademarks != null)
                {
                    return trademarks;
                }
                return new List<Trademark>();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Faild to get trademarks (Repository): {ex}");
                throw;
            }
        }

        public List<Trademark> GetTrademarksInSearchedText(string searchedText)
        {
            try
            {
                var trademarks = _ctx.Trademarks
                    .Where((Trademark trademark) => 
                        searchedText.ToLower().Contains(trademark.MarkVerbalElementText.ToLower()) && 
                        !trademark.MarkVerbalElementText.ToLower().Equals(searchedText.ToLower()) && 
                        trademark.MarkFeature.Equals("Word"))
                    .ToList<Trademark>();

                if (trademarks != null)
                {
                    return trademarks;
                }
                return new List<Trademark>();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Faild to get trademarks (Repository): {ex}");
                throw;
            }
        }

        public List<Trademark> GetAllTrademarks()
        {
            try
            {
                var trademarks = _ctx.Trademarks.ToList<Trademark>();

                if (trademarks != null)
                {
                    return trademarks;
                }
                return new List<Trademark>();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Faild to get trademarks (Repository): {ex}");
                throw;
            }
        }
    }
}
