﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace trademark_validation.DTOs
{
    public class TrademarkDTO
    {
        public string MarkFeature { get; set; }
        public string MarkVerbalElementText { get; set; }
        public string MarkCurrentStatusCode { get; set; }
        public DateTime? MarkCurrentStatusDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string PriorityNumber { get; set; }
        public string ApplicationLanguageCode { get; set; }
        public string State { get; set; }
    }
}
