﻿using AutoMapper;
using trademark_validation.Models;

namespace trademark_validation.DTOs
{
    public class TrademarkMappingProfile : Profile
    {
        public TrademarkMappingProfile()
        {
            CreateMap<Trademark, TrademarkDTO>().ReverseMap();
        }
    }
}
