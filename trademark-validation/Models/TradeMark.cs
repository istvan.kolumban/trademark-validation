﻿using System;
using System.ComponentModel.DataAnnotations;

namespace trademark_validation.Models
{
    public class Trademark
    {
        public int Id { get; set; }
        public string ApplicationNumber { get; set; }
        public string MarkFeature { get; set; }
        public string MarkVerbalElementText { get; set; }
        public string MarkCurrentStatusCode { get; set; }
        public DateTime? MarkCurrentStatusDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string PriorityNumber { get; set; }
        public string ApplicationLanguageCode { get; set; }
    }
}
