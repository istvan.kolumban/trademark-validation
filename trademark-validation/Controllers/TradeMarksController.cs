﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using trademark_validation.DTOs;
using trademark_validation.Models;
using trademark_validation.Services;

namespace trademark_validation.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TrademarksController : Controller
    {
        private readonly ITrademarksService _trademarksService;
        private readonly ILogger<TrademarksController> _logger;

        public TrademarksController(ITrademarksService trademarksService, ILogger<TrademarksController> logger)
        {
            _trademarksService = trademarksService;
            _logger = logger;
        }

        /// <summary>
        /// Searches for <see cref="Trademark"/>s which have exact match with the given text.
        /// </summary>
        /// <param name="searchedText">used for decide exact match</param>
        /// <returns>List of found <see cref="TrademarkDTO"/>s</returns>
        [HttpGet("exact/{searchedText}")]
        public ActionResult<List<TrademarkDTO>> GetExactMatches(string searchedText)
        {
            try
            {
                List<TrademarkDTO> trademarksDTOs = _trademarksService.GetExactMatches(searchedText);
                return Ok(trademarksDTOs);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Faild to get trademarks: {ex}");
                return BadRequest($"Faild to get trademarks (Controller): {ex}");
            }
        }

        /// <summary>
        /// Searches for <see cref="Trademark"/>s which have nearest match with the given text.
        /// </summary>
        /// <param name="searchedText">used for decide nearest matches</param>
        /// <returns>List of found <see cref="TrademarkDTO"/>s</returns>
        [HttpGet("nearest/{searchedText}")]
        public ActionResult<SortedList<int, List<TrademarkDTO>>> GetNearestMatches(string searchedText)
        {
            try
            {
                SortedList<int, List<TrademarkDTO>> trademarksDTOs = _trademarksService.GetNearestMatches(searchedText);
                return Ok(trademarksDTOs);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Faild to get trademarks: {ex}");
                return BadRequest($"Faild to get trademarks (Controller): {ex}");
            }
        }

        /// <summary>
        /// A trademark will be returned if 
        /// - it contains the searched text, 
        /// - the searched text contains the trademark, or
        /// - they are equals in case of no case sensitivity.
        /// </summary>
        /// <param name="searchedText">used for decide exact match</param>
        /// <returns>List of found <see cref="TrademarkDTO"/>s</returns>
        [HttpGet("contains/{searchedText}")]
        public ActionResult<Dictionary<string, List<TrademarkDTO>>> GetContainsMatches(string searchedText)
        {
            try
            {
                Dictionary<string, List<TrademarkDTO>> trademarksDTOs = _trademarksService.GetContainsMatches(searchedText);
                return Ok(trademarksDTOs);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Faild to get trademarks: {ex}");
                return BadRequest($"Faild to get trademarks (Controller): {ex}");
            }
        }
    }
}
