-- Create Database if not exist
IF NOT EXISTS(SELECT * FROM sys.databases WHERE name = 'trademark_db')
BEGIN
	CREATE DATABASE trademark_db
END
GO
    USE trademark_db
GO

-- Create table if not exist
IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='Trademarks_v2' and xtype='U')
BEGIN
    CREATE TABLE TradeMarks_v2 (
        Id INT PRIMARY KEY IDENTITY (1, 1) NOT NULL,
        ApplicationNumber VARCHAR(20) NOT NULL,
        MarkFeature VARCHAR(100) NOT NULL,
		MarkVerbalElementText VARCHAR(200) NOT NULL,
		MarkCurrentStatusCode VARCHAR(200),
		MarkCurrentStatusDate DATE,
		ExpiryDate DATE,
		PriorityNumber VARCHAR(30),
		ApplicationLanguageCode VARCHAR(10)
    )
END

-- Create temp table to list all files to process
DECLARE @subdirectory AS VARCHAR(200)='e:\Informatika\vacuumlabs\Interview 3\trademarks\preprocessed-trademarks\';

IF OBJECT_ID('tempdb..#DirectoryTree')IS NOT NULL
      DROP TABLE #DirectoryTree;
	  
CREATE TABLE #DirectoryTree (
      id int IDENTITY(1,1),
      subdirectory nvarchar(512),
      depth int,
      isfile bit,
	  processed bit DEFAULT 0);
  
INSERT #DirectoryTree (subdirectory, depth, isfile)
EXEC .xp_dirtree @subdirectory,1,1;

-- populate files one by one
DECLARE @Id INT;
DECLARE @NextFileName VARCHAR(200);
DECLARE @NextFileNameWithPath VARCHAR(200);

While EXISTS(SELECT * FROM #DirectoryTree WHERE isfile = 1 AND RIGHT(subdirectory,4) = '.xml' AND Processed = 0)
BEGIN
    Select TOP 1 @Id = Id, @NextFileName = subdirectory FROM #DirectoryTree WHERE isfile = 1 AND RIGHT(subdirectory,4) = '.xml' AND Processed = 0

	SET @NextFileNameWithPath = Concat(@subdirectory, @NextFileName);
	DECLARE @OpenRowSetSQL NVARCHAR(500);
	
	DECLARE @v_xmlfile XML;
	SET @OpenRowSetSQL=N'SELECT @v_xmlfile= CONVERT(XML, BulkColumn) FROM OPENROWSET (BULK '''+ @NextFileNameWithPath+''', SINGLE_BLOB) AS x;';
	EXEC sp_executesql @OpenRowSetSQL,N'@v_xmlfile xml output',@v_xmlfile OUTPUT;

	-- Collect the record datas

	DECLARE @ApplicationNumber VARCHAR(20);
	SET @ApplicationNumber = (SELECT  
		x.r.value('(ApplicationNumber)[1]', 'VARCHAR(20)') AS ApplicationNumber
		FROM    @v_xmlfile.nodes('/Transaction/TradeMarkTransactionBody/TransactionContentDetails/TransactionData/TradeMarkDetails/TradeMark') AS x(r));
	
	DECLARE @MarkFeature VARCHAR(100);
	SET @MarkFeature = (SELECT  
		x.r.value('(MarkFeature)[1]', 'VARCHAR(100)') AS MarkFeature
		FROM    @v_xmlfile.nodes('/Transaction/TradeMarkTransactionBody/TransactionContentDetails/TransactionData/TradeMarkDetails/TradeMark') AS x(r));

	DECLARE @MarkVerbalElementText VARCHAR(200);
	SET @MarkVerbalElementText = (SELECT  
		x.r.value('(MarkVerbalElementText)[1]', 'VARCHAR(200)') AS MarkVerbalElementText
		FROM    @v_xmlfile.nodes('/Transaction/TradeMarkTransactionBody/TransactionContentDetails/TransactionData/TradeMarkDetails/TradeMark/WordMarkSpecification') AS x(r));

	DECLARE @MarkCurrentStatusCode VARCHAR(200);
	SET @MarkCurrentStatusCode = (SELECT  
		x.r.value('(MarkCurrentStatusCode)[1]', 'VARCHAR(200)') AS MarkCurrentStatusCode
		FROM    @v_xmlfile.nodes('/Transaction/TradeMarkTransactionBody/TransactionContentDetails/TransactionData/TradeMarkDetails/TradeMark') AS x(r));

	DECLARE @MarkCurrentStatusDate DATE;
	SET @MarkCurrentStatusDate = (SELECT  
		x.r.value('(MarkCurrentStatusDate)[1]', 'DATE') AS MarkCurrentStatusDate
		FROM    @v_xmlfile.nodes('/Transaction/TradeMarkTransactionBody/TransactionContentDetails/TransactionData/TradeMarkDetails/TradeMark') AS x(r));

	DECLARE @ExpiryDate DATE;
	SET @ExpiryDate = (SELECT  
		x.r.value('(ExpiryDate)[1]', 'DATE') AS ExpiryDate
		FROM    @v_xmlfile.nodes('/Transaction/TradeMarkTransactionBody/TransactionContentDetails/TransactionData/TradeMarkDetails/TradeMark') AS x(r));

	DECLARE @PriorityNumber VARCHAR(30);
	SET @PriorityNumber = (SELECT  
		x.r.value('(PriorityNumber)[1]', 'VARCHAR(30)') AS PriorityNumber
		FROM    @v_xmlfile.nodes('/Transaction/TradeMarkTransactionBody/TransactionContentDetails/TransactionData/TradeMarkDetails/TradeMark/PriorityDetails/Priority') AS x(r));

	DECLARE @ApplicationLanguageCode VARCHAR(10);
	SET @ApplicationLanguageCode = (SELECT  
		x.r.value('(ApplicationLanguageCode)[1]', 'VARCHAR(10)') AS ApplicationLanguageCode
		FROM    @v_xmlfile.nodes('/Transaction/TradeMarkTransactionBody/TransactionContentDetails/TransactionData/TradeMarkDetails/TradeMark') AS x(r));

	-- insert into table
	INSERT INTO  dbo.Trademarks_v2 (ApplicationNumber, MarkFeature, MarkVerbalElementText, MarkCurrentStatusCode, MarkCurrentStatusDate, ExpiryDate, PriorityNumber, ApplicationLanguageCode)
	VALUES (@ApplicationNumber, @MarkFeature, @MarkVerbalElementText, @MarkCurrentStatusCode, @MarkCurrentStatusDate, @ExpiryDate, @PriorityNumber, @ApplicationLanguageCode);
	
	-- update temp table
    Update #DirectoryTree Set Processed = 1 Where Id = @Id;
End